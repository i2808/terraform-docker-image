FROM oraclelinux:8-slim

RUN microdnf install git jq wget unzip \
    && TF_VERSION=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r .current_version?) \
    && wget -O terraform.zip https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip \
    && unzip terraform.zip terraform -d /usr/bin/
